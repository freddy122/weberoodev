<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'proj_dev');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'cttO91<-3;ig7w3U35KL=wG9UeEZ0Qu{B.CqD;Dm`N9L[;G<j&QczmfnF:&R6n1V');
define('SECURE_AUTH_KEY',  'HB,#^3)69%[8/#gT!^`wq@,vpd<6=_h3Nji?7]X:%lmNAHA*Yr| VoT]]oWx{2;z');
define('LOGGED_IN_KEY',    '6v+@5g^rPHg9m[T>F@Rh1cvgQxE|hM6,`gdT?Mu7,qu[:1j2)4z;KRGxJ|s.]pue');
define('NONCE_KEY',        'c~fnq3:zQ]>I{N<2.Jkwowxa,aZ8OER>h4t[^F%Wjw/a8:}ceZ3jK-DR%0O?v@Md');
define('AUTH_SALT',        'SAXkx}R(oof1~SHIQ{%in{l38!?WTX^lqP<AkUJ;OY!.;[C,/DL*/2QxdN,k`F%c');
define('SECURE_AUTH_SALT', '9&eEeb;kS/<&DB^xv^PdK;_?2Ce^IEHwE,5 +bWfIix$Wruv2rDIIQs[h;{;z<D)');
define('LOGGED_IN_SALT',   '{_4Tkx+=z/:UyA>{27e3>HO4q)X>MpoG~P3bx!xUR:A.^N,~neM}|/!EMFE~r&q0');
define('NONCE_SALT',       'R/6LVbJT:%k5lSIYK4!$h57FHE-Z(x^LRWRJ~lP$+.~_Lj0j$AM9K8|5KcRikTFv');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');