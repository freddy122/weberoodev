<?php
include( dirname( __FILE__ ) . '/wp-load.php' );

global $wpdb;
require_once 'vendor/autoload.php';
$infusionsoft = new \Infusionsoft\Infusionsoft(
    array(
            'clientId'     => 'uj3xp8vchkm87th38ttkr7pr',
            'clientSecret' => 'K37DzytKQr',
            'redirectUri'  => 'http://duroy.webero.fr/teste-liste-contact/',
    )
);
$sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
$resMaxId = $sqlMaxId->ids;
$infusionrow    = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
$accessToken    = $infusionrow->tokens;
$refreshToken   = $infusionrow->tokens_refresh;
$old_token = new \Infusionsoft\Token();
$old_token->setAccessToken($accessToken);
$old_token->setRefreshToken($refreshToken);
$infusionsoft->setToken($old_token);
$infusionsoft->refreshAccessToken();
$infstToken = $infusionsoft->getToken();
$dateNow = date('Y-m-d H:i:s');
$data =array(
    'tokens'=>$infstToken->getAccessToken(),
    'tokens_refresh'=>$infstToken->getRefreshToken(),
    'lifetime'=>$infstToken->getEndOfLife(),
    'updated_at'=>$dateNow
);
$where = array('id'=>(int)$resMaxId);
$wpdb->update($wpdb->prefix.'tokens', $data, $where);
