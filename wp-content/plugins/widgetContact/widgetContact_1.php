<?php
/*
Plugin Name: Widget information contact
Description: Widget information de contact
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/

function wpb_load_widget_contact() {
    register_widget( 'detail_contact_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget_contact' );

function connect_to_infusion_soft_contacts(){
    global $wpdb;
    include ABSPATH . "/vendor/autoload.php";
    $infusionsoft = new \Infusionsoft\Infusionsoft(array(
        'clientId'     => 'uj3xp8vchkm87th38ttkr7pr',
        'clientSecret' => 'K37DzytKQr',
        'redirectUri'  => 'http://duroy.webero.fr/teste-liste-contact/',
    ));
    $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
    $resMaxId = $sqlMaxId->ids;
    $infusionrow    = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
    $accessToken    = $infusionrow->tokens;
    $refreshToken   = $infusionrow->tokens_refresh;
    $endtimeToken   = (int)$infusionrow->lifetime;
    $token_in_database = new \Infusionsoft\Token();
    $token_in_database->setAccessToken($accessToken);
    $token_in_database->setRefreshToken($refreshToken);
    $token_in_database->setEndOfLife($endtimeToken);
    $infusionsoft->setToken($token_in_database);
    return $infusionsoft;
}

function get_contact_by_id($contactID){
    $infusionsoft   = connect_to_infusion_soft_contacts();
    $returnFields   = array(
        /************** general ************/
        /* general information */
        'FirstName',
        'LastName',
        'Company',
        'JobTitle',
        'ContactType',
        'LeadSourceId',
        'OwnerID',
        
        /* phone & fax */
        'Phone1Type',
        'Phone1',
        'Phone1Ext',
        'Phone2Type',
        'Phone2',
        'Phone2Ext',
        'Fax1Type',
        'Fax1',
        
        /* email social */
        'Email',
        'Website',
        //'SocialAccount',
        
        /* global info */
        'LanguageTag',
        'TimeZone',
        /************** fin general ************/
        
        /************** adresse ************/
        /* adresse 1 */
        'StreetAddress1',
        'StreetAddress2',
        'City',
        'State',
        'PostalCode',
        'ZipFour1',
        'Country',
        
        /* adresse 2 */
        'Address3Street1',
        'Address3Street2',
        'City3',
        'State3',
        'PostalCode3',
        'ZipFour3',
        'Country3',
        /************** fin adresse ************/
        
        /************** additional info ************/
        /* other fields 1  */
        'AssistantName',
        'AssistantPhone',
        //'SSN',
        'Birthday',
        'SpouseName',
        'Anniversary',
        'Title',
        'Suffix',
        'MiddleName',
        'Nickname',
        /* other fields 2  */
        'ReferralCode',
        'Phone3Type',
        'Phone3',
        'Phone3Ext',
        'Phone4Type',
        'Phone4',
        'Phone4Ext',
        'Phone5Type',
        'Phone5',
        'Phone5Ext',
        'Fax2Type',
        'Fax2',
        'EmailAddress2',
        'EmailAddress3',
        'Username',
        'Password',
        /************** fin additional info ************/
        
        /************** Person note ************/
        'ContactNotes',
        /************** fin Person note ************/
        
        /************** Devis personnalisé ****************/
        /* Appareils et remises */
        '_Appareilsàafficher',
        '_Remiseaumscan30',
        '_Remiseaumscan40',
        '_Remisecardiaum0',
        
        /*Informations du devis AUTO */
        '_NumérodedevisprovientdeWPparAPI',
        '_DatedecréationdudevisprovientdeWPparAPI1erevisite',
        '_DatededernièreconsultationdudevisIDENTIQUEparWPapi',
        '_LiendacompteWPAPI',
        '_LiendusoldeapresacompteWPAPIText',
        '_LiendelatotalitéWPAPI',
        '_TvapartieappareilappliqueeAPIWEB',
        '_TvapartielicenceetformationappliqueeAPIWEB',
        '_Totaldelapartieappareil',
        '_Totaldelapartielicence',
        '_TotaldetoutHT',
        '_TotaldetoutTVAC',
        '_Voirledevisdelacheteurdanslenavigateur',
        
        /*Informations du devis VENDEUR*/
        '_DatedevaliditédudevisPardéfautsivide15JVENDEUR',
        '_CommentairepersonnalisésurledevisVENDEUR',
        '_AcheteurprofessionnelNdeTVA',
        '_AfficherlaTVAmêmesiclientproffessionel',
        '_Paysdelacheteur0',
        
        /************** fin Devis personnalisé ************/
        
        
        /************** Decompteur offre spécial ************/
        '_Datededebutdudecompte',
        '_Datedefindudecompte',
        '_Datededernièrevisitedudecompte',
        '_Nombredejourdeloffre0',
        /************** fin Decompteur offre spécial ************/
    );
    //$contact_id = isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"];
    $resdate        = $infusionsoft->contacts('xml')->load($contactID, $returnFields);
    return $resdate;
}

class detail_contact_widget extends WP_Widget {
 
    //private $contact_id = 13;
    private $contact_id;
    
    function __construct() {
        parent::__construct(
            'detail_contact_widget', 
            'Widget contact', 
            array( 'description' => "Widget permet de lister l'information de contact", ) 
        );
        
        $this->contact_id = isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"];
    }
    
    // Affichage du widget dans front office
    public function widget( $args, $instance ){
        $nombre_de_jour                = apply_filters( 'widget_title', $instance['title'] );
        if(isset($this->contact_id)){
            $contactDetail =  get_contact_by_id($this->contact_id);
            
            $general_information = array(
                "FirstName" => $contactDetail["FirstName"],
                "" => $contactDetail[""],
                "" => $contactDetail[""],
                "" => $contactDetail[""],
                "" => $contactDetail[""],
                "" => $contactDetail[""],
            );
            var_dump($contactDetail);
            die();
            echo $args['before_widget'];
            echo $args['before_title'] . $nombre_de_jour . $args['after_title'];
            require dirname( __FILE__ ) . '/template/index.php';
            echo $args['after_widget'];
        }
    }

    // Affichage Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'input:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
    
    // Mise à jour 
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} 