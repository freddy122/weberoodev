<?php
/*
Plugin Name: Widget information contact
Description: Widget information de contact
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/

function wpb_load_widget_contact() {
    register_widget( 'detail_contact_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget_contact' );

function connect_to_infusion_soft_contacts(){
    global $wpdb;
    include "../../../vendor/autoload.php";
    $infusionsoft = new \Infusionsoft\Infusionsoft(array(
        'clientId'     => 'jdfehyn2gwa23knh9ygj4gdj',
            'clientSecret' => 'wJ5mqRn9HE',
            'redirectUri'  => 'https://bio-resonance.eu/',
    ));
    $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
    $resMaxId = $sqlMaxId->ids;
    $infusionrow    = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
    $accessToken    = $infusionrow->tokens;
    $refreshToken   = $infusionrow->tokens_refresh;
    $endtimeToken   = (int)$infusionrow->lifetime;
    $token_in_database = new \Infusionsoft\Token();
    $token_in_database->setAccessToken($accessToken);
    $token_in_database->setRefreshToken($refreshToken);
    $token_in_database->setEndOfLife($endtimeToken);
    $infusionsoft->setToken($token_in_database);
    return $infusionsoft;
}

function get_contact_by_id($contactID){
    $infusionsoft   = connect_to_infusion_soft_contacts();
    var_dump($infusionsoft);die();
    $returnFields   = array(
        /************** general ************/
        /* general information */
        'FirstName',
        'LastName',
        'Company',
        'JobTitle',
        'ContactType',
        'LeadSourceId',
        'OwnerID',
        
        /* phone & fax */
        'Phone1Type',
        'Phone1',
        'Phone1Ext',
        'Phone2Type',
        'Phone2',
        'Phone2Ext',
        'Fax1Type',
        'Fax1',
        
        /* email social */
        'Email',
        'Website',
        //'SocialAccount',
        
        'LanguageTag',
        'TimeZone',
        /************** fin general ************/
        
        /************** adresse ************/
        /* adresse 1 */
        'StreetAddress1',
        'StreetAddress2',
        'City',
        'State',
        'PostalCode',
        'ZipFour1',
        'Country',
        
        /* adresse 2 */
        'Address3Street1',
        'Address3Street2',
        'City3',
        'State3',
        'PostalCode3',
        'ZipFour3',
        'Country3',
        /************** fin adresse ************/
        
        /************** additional info ************/
        /* other fields 1  */
        'AssistantName',
        'AssistantPhone',
        //'SSN',
        'Birthday',
        'SpouseName',
        'Anniversary',
        'Title',
        'Suffix',
        'MiddleName',
        'Nickname',
        /* other fields 2  */
        'ReferralCode',
        'Phone3Type',
        'Phone3',
        'Phone3Ext',
        'Phone4Type',
        'Phone4',
        'Phone4Ext',
        'Phone5Type',
        'Phone5',
        'Phone5Ext',
        'Fax2Type',
        'Fax2',
        'EmailAddress2',
        'EmailAddress3',
        'Username',
        'Password',
        /************** fin additional info ************/
        
        /************** Person note ************/
        'ContactNotes',
        /************** fin Person note ************/
        
        /************** Devis personnalisé ****************/
        /* Appareils et remises */
        '_Appareilsàafficher',
        '_Remiseaumscan30',
        '_Remiseaumscan40',
        '_Remisecardiaum0',
        
        /*Informations du devis AUTO */
        '_NumérodedevisprovientdeWPparAPI',
        '_DatedecréationdudevisprovientdeWPparAPI1erevisite',
        '_DatededernièreconsultationdudevisIDENTIQUEparWPapi',
        '_LiendacompteWPAPI',
        '_LiendusoldeapresacompteWPAPIText',
        '_LiendelatotalitéWPAPI',
        '_TvapartieappareilappliqueeAPIWEB',
        '_TvapartielicenceetformationappliqueeAPIWEB',
        '_Totaldelapartieappareil',
        '_Totaldelapartielicence',
        '_TotaldetoutHT',
        '_TotaldetoutTVAC',
        '_Voirledevisdelacheteurdanslenavigateur',
        
        /*Informations du devis VENDEUR*/
        '_DatedevaliditédudevisPardéfautsivide15JVENDEUR',
        '_CommentairepersonnalisésurledevisVENDEUR',
        '_AcheteurprofessionnelNdeTVA',
        '_AfficherlaTVAmêmesiclientproffessionel',
        '_Paysdelacheteur0',
        
        /************** fin Devis personnalisé ************/
        
        
        /************** Decompteur offre spécial ************/
        '_Datededebutdudecompte',
        '_Datededebutdudecompte',
        '_Datedefindudecompte',
        '_Datedefindudecompte',
        '_Datededernièrevisitedudecompte',
        '_Datededernièrevisitedudecompte',
        '_Nombredejourdeloffre0',
        /************** fin Decompteur offre spécial ************/
    );
    //$contact_id = isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"];
    $resdate        = $infusionsoft->contacts('xml')->load($contactID, $returnFields);
    return $resdate;
}

function get_all_tags($contact_id){
    global $wpdb;
    $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
    $resMaxId = $sqlMaxId->ids;
    $infusionrow = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
    $accessToken = $infusionrow->tokens;
    $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts/".$contact_id."/tags?limit=1000&access_token=".$accessToken;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$urls);
    $result=curl_exec($ch);
    curl_close($ch);
    $resu = (json_decode($result, true));
    return $resu;
}

class detail_contact_widget extends WP_Widget {
 
    //private $contact_id = 13;
    private $contact_id;
    public $contactDetail;
    function __construct() {
        parent::__construct(
            'detail_contact_widget', 
            'Widget contact', 
            array( 'description' => "Widget permet de lister l'information de contact", ) 
        );
        
        $this->contact_id = isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"];
    }
    
    public function get_general_information(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        //var_dump($contactDetail);
        // onglet general
        $general_information = array(
            "FirstName" => !empty($contactDetail["FirstName"])?$contactDetail["FirstName"]:"",
            "LastName" => !empty($contactDetail["LastName"])?$contactDetail["LastName"]:"",
            "Company" => !empty($contactDetail["Company"])?$contactDetail["Company"]:"",
            "JobTitle" => !empty($contactDetail["JobTitle"])?$contactDetail["JobTitle"]:"",
            "ContactType" => !empty($contactDetail["ContactType"])?$contactDetail["ContactType"]:"",
            "LeadSourceId" => !empty($contactDetail["LeadSourceId"])?$contactDetail["LeadSourceId"]:"",
            "OwnerID" => !empty($contactDetail["OwnerID"])?$contactDetail["OwnerID"]:"",
        );
        return $general_information;
    }
    
    public function get_phone_fax(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $phone_fax = array(
                "Phone1Type" => !empty($contactDetail["Phone1Type"])?$contactDetail["Phone1Type"]:"",
                "Phone1" => !empty($contactDetail["Phone1"])?$contactDetail["Phone1"]:"",
                "Phone1Ext" => !empty($contactDetail["Phone1Ext"])?$contactDetail["Phone1Ext"]:"",
                "Phone2Type" => !empty($contactDetail["Phone2Type"])?$contactDetail["Phone2Type"]:"",
                "Phone2" => !empty($contactDetail["Phone2"])?$contactDetail["Phone2"]:"",
                "Phone2Ext" => !empty($contactDetail["Phone2Ext"])?$contactDetail["Phone2Ext"]:"",
                "Fax1Type" => !empty($contactDetail["Fax1Type"])?$contactDetail["Fax1Type"]:"",
                "Fax1" => !empty($contactDetail["Fax1"])?$contactDetail["Fax1"]:"",
            );
        return $phone_fax;
    }
    
    public function get_email_info(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $email_info = array(
                "Email" => !empty($contactDetail["Email"])?$contactDetail["Email"]:"",
                "Website" => !empty($contactDetail["Website"])?$contactDetail["Website"]:"",
            );
        return $email_info;
    }
    
    public function get_global_info(){
       $contactDetail =  get_contact_by_id($this->contact_id);
        $global_info = array(
                "LanguageTag" => !empty($contactDetail["LanguageTag"])?$contactDetail["LanguageTag"]:"",
                "TimeZone" => !empty($contactDetail["TimeZone"])?$contactDetail["TimeZone"]:"",
            );
        return $global_info;
    }
    
    public function get_adresse_1(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $adresse_1 = array(
                "StreetAddress1" => !empty($contactDetail["StreetAddress1"])?$contactDetail["StreetAddress1"]:"",
                "StreetAddress2" => !empty($contactDetail["StreetAddress2"])?$contactDetail["StreetAddress2"]:"",
                "City" => !empty($contactDetail["City"])?$contactDetail["City"]:"",
                "State" => !empty($contactDetail["State"])?$contactDetail["State"]:"",
                "PostalCode" => !empty($contactDetail["PostalCode"])?$contactDetail["PostalCode"]:"",
                "ZipFour1" => !empty($contactDetail["ZipFour1"])?$contactDetail["ZipFour1"]:"",
                "Country" => !empty($contactDetail["Country"])?$contactDetail["Country"]:"",
            );
        return $adresse_1;
    }
    public function get_adresse_2(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $adresse_2 = array(
                "Address3Street1" => !empty($contactDetail["Address3Street1"])?$contactDetail["Address3Street1"]:"",
                "Address3Street2" => !empty($contactDetail["Address3Street2"])?$contactDetail["Address3Street2"]:"",
                "City3" => !empty($contactDetail["City3"])?$contactDetail["City3"]:"",
                "State3" => !empty($contactDetail["State3"])?$contactDetail["State3"]:"",
                "PostalCode3" => !empty($contactDetail["PostalCode3"])?$contactDetail["PostalCode3"]:"",
                "ZipFour3" => !empty($contactDetail["ZipFour3"])?$contactDetail["ZipFour3"]:"",
                "Country3" => !empty($contactDetail["Country3"])?$contactDetail["Country3"]:"",
            );
        return $adresse_2;
    }
    public function get_other_field1(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $other_field1 = array(
                "AssistantName" => !empty($contactDetail["AssistantName"])?$contactDetail["AssistantName"]:"",
                "AssistantPhone" => !empty($contactDetail["AssistantPhone"])?$contactDetail["AssistantPhone"]:"",
                "Birthday" => !empty($contactDetail["Birthday"])?$contactDetail["Birthday"]:"",
                "SpouseName" => !empty($contactDetail["SpouseName"])?$contactDetail["SpouseName"]:"",
                "Anniversary" => !empty($contactDetail["Anniversary"])?$contactDetail["Anniversary"]:"",
                "Title" => !empty($contactDetail["Title"])?$contactDetail["Title"]:"",
                "Suffix" => !empty($contactDetail["Suffix"])?$contactDetail["Suffix"]:"",
                "MiddleName" => !empty($contactDetail["MiddleName"])?$contactDetail["MiddleName"]:"",
                "Nickname" => !empty($contactDetail["Nickname"])?$contactDetail["Nickname"]:"",
            );
        return $other_field1;
    }
    public function get_other_field2(){
       $contactDetail =  get_contact_by_id($this->contact_id);
        $other_field2 = array(
                "ReferralCode" => !empty($contactDetail["ReferralCode"])?$contactDetail["ReferralCode"]:"",
                "Phone3Type" => !empty($contactDetail["Phone3Type"])?$contactDetail["Phone3Type"]:"",
                "Phone3" => !empty($contactDetail["Phone3"])?$contactDetail["Phone3"]:"",
                "Phone3Ext" => !empty($contactDetail["Phone3Ext"])?$contactDetail["Phone3Ext"]:"",
                "Phone4Type" => !empty($contactDetail["Phone4Type"])?$contactDetail["Phone4Type"]:"",
                "Phone4" => !empty($contactDetail["Phone4"])?$contactDetail["Phone4"]:"",
                "Phone4Ext" => !empty($contactDetail["Phone4Ext"])?$contactDetail["Phone4Ext"]:"",
                "Phone5Type" => !empty($contactDetail["Phone5Type"])?$contactDetail["Phone5Type"]:"",
                "Phone5" => !empty($contactDetail["Phone5"])?$contactDetail["Phone5"]:"",
                "Phone5Ext" => !empty($contactDetail["Phone5Ext"])?$contactDetail["Phone5Ext"]:"",
                "Fax2Type" => !empty($contactDetail["Fax2Type"])?$contactDetail["Fax2Type"]:"",
                "Fax2" => !empty($contactDetail["Fax2"])?$contactDetail["Fax2"]:"",
                "EmailAddress2" => !empty($contactDetail["EmailAddress2"])?$contactDetail["EmailAddress2"]:"",
                "EmailAddress3" => !empty($contactDetail["EmailAddress3"])?$contactDetail["EmailAddress3"]:"",
                "Username" => !empty($contactDetail["Username"])?$contactDetail["Username"]:"",
                "Password" => !empty($contactDetail["Password"])?$contactDetail["Password"]:"",
            );
        return $other_field2;
    }
    public function get_person_note(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $person_note = array(
                "ContactNotes" => !empty($contactDetail["ContactNotes"])?$contactDetail["ContactNotes"]:"",
            );
        return $person_note;
    }
    public function get_appareil_et_remise(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $appareil_et_remise = array(
                "_Appareilsàafficher" => !empty($contactDetail["_Appareilsàafficher"])?$contactDetail["_Appareilsàafficher"]:"",
                "_Remiseaumscan30" => !empty($contactDetail["_Remiseaumscan30"])?$contactDetail["_Remiseaumscan30"]:"",
                "_Remiseaumscan40" => !empty($contactDetail["_Remiseaumscan40"])?$contactDetail["_Remiseaumscan40"]:"",
                "_Remisecardiaum0" => !empty($contactDetail["_Remisecardiaum0"])?$contactDetail["_Remisecardiaum0"]:"",
            );
        return $appareil_et_remise;
    }
    
    public function get_information_devis_auto(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $information_devis_auto = array(
                "_NumérodedevisprovientdeWPparAPI" => !empty($contactDetail["_NumérodedevisprovientdeWPparAPI"])?$contactDetail["_NumérodedevisprovientdeWPparAPI"]:"",
                "_DatedecréationdudevisprovientdeWPparAPI1erevisite" => !empty($contactDetail["_DatedecréationdudevisprovientdeWPparAPI1erevisite"])?$contactDetail["_DatedecréationdudevisprovientdeWPparAPI1erevisite"]:"",
                "_DatededernièreconsultationdudevisIDENTIQUEparWPapi" => !empty($contactDetail["_DatededernièreconsultationdudevisIDENTIQUEparWPapi"])?$contactDetail["_DatededernièreconsultationdudevisIDENTIQUEparWPapi"]:"",
                "_LiendacompteWPAPI" => !empty($contactDetail["_LiendacompteWPAPI"])?$contactDetail["_LiendacompteWPAPI"]:"",
                "_LiendusoldeapresacompteWPAPIText" => !empty($contactDetail["_LiendusoldeapresacompteWPAPIText"])?$contactDetail["_LiendusoldeapresacompteWPAPIText"]:"",
                "_LiendelatotalitéWPAPI" => !empty($contactDetail["_LiendelatotalitéWPAPI"])?$contactDetail["_LiendelatotalitéWPAPI"]:"",
                "_TvapartieappareilappliqueeAPIWEB" => !empty($contactDetail["_TvapartieappareilappliqueeAPIWEB"])?$contactDetail["_TvapartieappareilappliqueeAPIWEB"]:"",
                "_TvapartielicenceetformationappliqueeAPIWEB" => !empty($contactDetail["_TvapartielicenceetformationappliqueeAPIWEB"])?$contactDetail["_TvapartielicenceetformationappliqueeAPIWEB"]:"",
                "_Totaldelapartieappareil" => !empty($contactDetail["_Totaldelapartieappareil"])?$contactDetail["_Totaldelapartieappareil"]:"",
                "_Totaldelapartielicence" => !empty($contactDetail["_Totaldelapartielicence"])?$contactDetail["_Totaldelapartielicence"]:"",
                "_TotaldetoutHT" => !empty($contactDetail["_TotaldetoutHT"])?$contactDetail["_TotaldetoutHT"]:"",
                "_TotaldetoutTVAC" => !empty($contactDetail["_TotaldetoutTVAC"])?$contactDetail["_TotaldetoutTVAC"]:"",
                "_Voirledevisdelacheteurdanslenavigateur" => !empty($contactDetail["_Voirledevisdelacheteurdanslenavigateur"])?$contactDetail["_Voirledevisdelacheteurdanslenavigateur"]:"",
            );
        return $information_devis_auto;
    }
    
    public function get_information_du_devis_vendeur(){
        $contactDetail =  get_contact_by_id($this->contact_id);
         $information_du_devis_vendeur = array(
                "_DatedevaliditédudevisPardéfautsivide15JVENDEUR" => !empty($contactDetail["_DatedevaliditédudevisPardéfautsivide15JVENDEUR"])?$contactDetail["_DatedevaliditédudevisPardéfautsivide15JVENDEUR"]:"",
                "_CommentairepersonnalisésurledevisVENDEUR" => !empty($contactDetail["_CommentairepersonnalisésurledevisVENDEUR"])?$contactDetail["_CommentairepersonnalisésurledevisVENDEUR"]:"",
                "_AcheteurprofessionnelNdeTVA" => !empty($contactDetail["_AcheteurprofessionnelNdeTVA"])?$contactDetail["_AcheteurprofessionnelNdeTVA"]:"",
                "_AfficherlaTVAmêmesiclientproffessionel" => !empty($contactDetail["_AfficherlaTVAmêmesiclientproffessionel"])?$contactDetail["_AfficherlaTVAmêmesiclientproffessionel"]:"",
                "_Paysdelacheteur0" => !empty($contactDetail["_Paysdelacheteur0"])?$contactDetail["_Paysdelacheteur0"]:"",
            );
        return $information_du_devis_vendeur;
    }
    public function get_decompteur_offre_special(){
        $contactDetail =  get_contact_by_id($this->contact_id);
        $decompteur_offre_special = array(
                "_Datededebutdudecompte" => !empty($contactDetail["_Datededebutdudecompte"])?$contactDetail["_Datededebutdudecompte"]->format('d-m-Y h:i:s'):"",
                "_Datedefindudecompte" => !empty($contactDetail["_Datedefindudecompte"])? $contactDetail["_Datedefindudecompte"]->format('d-m-Y h:i:s'):"",
                "_Datededernièrevisitedudecompte" => !empty($contactDetail["_Datededernièrevisitedudecompte"])? $contactDetail["_Datededernièrevisitedudecompte"]->format('d-m-Y h:i:s'):"",
                "_Nombredejourdeloffre0" => !empty($contactDetail["_Nombredejourdeloffre0"])?$contactDetail["_Nombredejourdeloffre0"]:"",
            );
        return $decompteur_offre_special;
    }
    public function get_arr_applied_tag(){
        $arr_applied_tag = array();
            foreach(get_all_tags($this->contact_id)['tags'] as $appltag){
                $arr_applied_tag[] = array(
                    'id' => $appltag['tag']['id'],
                    'name' => $appltag['tag']['name'],
                    'category' => $appltag['tag']['category'],
                    'date_applied' => (new \DateTime($appltag['date_applied']))->format('d-m-Y H:s:i'),
                );
            }
        return $arr_applied_tag;
    }
    
    
    // Affichage du widget dans front office
    public function widget( $args, $instance ){
        $nombre_de_jour                = apply_filters( 'widget_title', $instance['title'] );
        if(isset($this->contact_id)){
            echo $args['before_widget'];
            echo $args['after_widget'];
        }
    }

    // Affichage Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'input:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
    
    // Mise à jour 
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} 