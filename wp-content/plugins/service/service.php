<?php
/*
Plugin Name:  Service
Description:  Service
Version:      0.0.1
Author:       freds
License:      GPL2
Text Domain:  wporg
Domain Path:  /languages
*/
if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Customers_List extends WP_List_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Customer', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Customers', 'sp' ), //plural name of the listed records
			'ajax'     => false //should this table support ajax?

		] );

}
public static function get_customers( $per_page = 5, $page_number = 1 ) {

  global $wpdb;

  $sql = "SELECT * FROM {$wpdb->prefix}posts where post_type = 'service' ";

  if ( ! empty( $_REQUEST['orderby'] ) ) {
    $sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
    $sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
  }

  $sql .= " LIMIT $per_page";

  $sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;


  $result = $wpdb->get_results( $sql, 'ARRAY_A' );

  return $result;
}

public static function delete_customer( $id ) {
  global $wpdb;

  $wpdb->delete(
    "{$wpdb->prefix}customers",
    [ 'ID' => $id ],
    [ '%d' ]
  );
}
public static function record_count() {
  global $wpdb;

  $sql = "SELECT COUNT(*) FROM {$wpdb->prefix}customers";

  return $wpdb->get_var( $sql );
}
public function no_items() {
  _e( 'No customers avaliable.', 'sp' );
}
function column_name( $item ) {

  // create a nonce
  $delete_nonce = wp_create_nonce( 'sp_delete_customer' );

  $title = '<strong>' . $item['post_title'] . '</strong>';

  $actions = [
    'delete' => sprintf( '<a href="?page=%s&action=%s&customer=%s&_wpnonce=%s">Delete</a>', esc_attr( $_REQUEST['page'] ), 'delete', absint( $item['ID'] ), $delete_nonce )
  ];

  return $title . $this->row_actions( $actions );
}
public function column_default( $item, $column_name ) {
  switch ( $column_name ) {
    case 'post_title':
    case 'post_content':
      return $item[ $column_name ];
    default:
      return print_r( $item, true ); //Show the whole array for troubleshooting purposes
  }
}
function column_cb( $item ) {
  return sprintf(
    '<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
  );
}

function get_columns() {
  $columns = [
    'ID'      => '<input type="checkbox" />',
    'post_title'    => __( 'post_title', 'sp' ),
    'post_content' => __( 'post_content', 'sp' ),
    'post_type'    => __( 'post_type', 'sp' )
  ];

  return $columns;
}

public function get_sortable_columns() {
  $sortable_columns = array(
    'post_title' => array( 'post_title', true ),
    'post_content' => array( 'post_content', false )
  );

  return $sortable_columns;
}
public function get_bulk_actions() {
  $actions = [
    'bulk-delete' => 'Delete'
  ];

  return $actions;
}

public function prepare_items() {

  $this->_column_headers = $this->get_column_info();

  /** Process bulk action */
  $this->process_bulk_action();

  $per_page     = $this->get_items_per_page( 'customers_per_page', 5 );
  $current_page = $this->get_pagenum();
  $total_items  = self::record_count();

  $this->set_pagination_args( [
    'total_items' => $total_items, //WE have to calculate the total number of items
    'per_page'    => $per_page //WE have to determine how many items to show on a page
  ] );


  $this->items = self::get_customers( $per_page, $current_page );
}

public function process_bulk_action() {

  //Detect when a bulk action is being triggered...
  if ( 'delete' === $this->current_action() ) {

    // In our file that handles the request, verify the nonce.
    $nonce = esc_attr( $_REQUEST['_wpnonce'] );

    if ( ! wp_verify_nonce( $nonce, 'sp_delete_customer' ) ) {
      die( 'Go get a life script kiddies' );
    }
    else {
      self::delete_customer( absint( $_GET['customer'] ) );

      wp_redirect( esc_url( add_query_arg() ) );
      exit;
    }

  }

  // If the delete bulk action is triggered
  if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
       || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
  ) {

    $delete_ids = esc_sql( $_POST['bulk-delete'] );

    // loop over the array of record IDs and delete them
    foreach ( $delete_ids as $id ) {
      self::delete_customer( $id );

    }

    wp_redirect( esc_url( add_query_arg() ) );
    exit;
  }
}

        }
        
        
class SP_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
}
public static function set_screen( $status, $option, $value ) {
	return $value;
}

public function plugin_menu() {

	$hook = add_menu_page(
		'Sitepoint WP_List_Table Example',
		'blabalbla',
		'manage_options',
		'wp_list_table_class',
		[ $this, 'plugin_settings_page' ]
	);

	add_action( "load-$hook", [ $this, 'screen_option' ] );

}
public function screen_option() {

	$option = 'per_page';
	$args   = [
		'label'   => 'Customers',
		'default' => 5,
		'option'  => 'customers_per_page'
	];

	add_screen_option( $option, $args );

	$this->customers_obj = new Customers_List();
}
public static function get_instance() {
	if ( ! isset( self::$instance ) ) {
		self::$instance = new self();
	}

	return self::$instance;
}
public function plugin_settings_page() {
	?>
	<div class="wrap">
		<h2>WP_List_Table Class Example</h2>

		<div id="poststuff">
			<div id="post-body" class="metabox-holder columns-2">
				<div id="post-body-content">
					<div class="meta-box-sortables ui-sortable">
						<form method="post">
							<?php
							$this->customers_obj->prepare_items();
							$this->customers_obj->display(); ?>
						</form>
					</div>
				</div>
			</div>
			<br class="clear">
		</div>
	</div>
<?php
}
        }


add_shortcode("service", "afficher_service");
function afficher_service(){
    if(!is_admin()){
        echo "fesfefaaa";
    }
}



add_action( 'admin_menu', 'my_admin_menu' );
function my_admin_menu() {
	add_menu_page( 'My Top Level Menu Example', 'Top Level Menu', 'manage_options', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-tickets', 6  );
}
add_action( 'plugins_loaded', function () {
	SP_Plugin::get_instance();
} );
function myplguin_admin_page(){
    ?>
    <div class="wrap">
            <h2>Welcome To My Plugin</h2>
    </div>
    <?php
}
