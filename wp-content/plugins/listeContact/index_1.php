
<div id="clockdiv">
  <div>
    <span class="jours" id="jours">7</span>
    <div class="smalltext">Jours</div>
  </div>
  <div>
    <span class="heures" id="heures">12</span>
    <div class="smalltext">Heures</div>
  </div>
  <div>
    <span class="minutes" id="minutes">60</span>
    <div class="smalltext">Minutes</div>
  </div>
  <div>
    <span class="secondes" id="secondes">60</span>
    <div class="smalltext">Secondes</div>
  </div>
</div>

<div id="fin"></div>
<style>
#demo{
    display: none;
}
h1{
  color: #396;
  font-weight: 100;
  font-size: 30px;
  margin: 40px 0px 20px;
}

#clockdiv{
	font-family: sans-serif;
	color: #fff;
	display: inline-block;
	font-weight: 100;
	text-align: center;
	font-size: 26px;
	width: 100%;
}

#clockdiv > div{
	padding: 10px;
	border-radius: 3px;
	background: #4584CC;
	display: inline-block;
}

#clockdiv div > span{
	padding: 10px;
	border-radius: 3px;
	background: #0056B9;
	display: inline-block;
}

.smalltext{
	padding-top: 5px;
	font-size: 12px;
}
</style>
<script>
//GET cookie
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //SET cookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        console.log('expirations : '+expires);
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
    
    function beforeMidnight(){
       //var mid= new Date(); (ancien code)
        var mid = new Date(<?php echo time(); ?>);
        ts= mid.getTime();
        mid.setHours(24, 0, 0, 0);
        return Math.floor((mid - ts));
    }
    
    //debut code 
    //var now = new Date().getTime();(ancien code)
    var now = new Date(<?php echo strtotime("now"); ?>).getTime();
    var countDownDate = now;
    var countDownDate = getCookie("datedefinnum");
    //var countDownDate = String(new Date(<?php echo $default_date_fin; ?>).getTime());
    //alert (beforeMidnight());
    restejournee = beforeMidnight();
    //alert (restejournee);
   
    var days = <?php echo $nombre_de_jour; ?>;
    var date = new Date(<?php //echo $default_date_fin; ?>).getTime();
    /* ancien code
     * var res = restejournee + date + (days * 24 * 60 * 60 * 1000);
    var datedefin = new Date(res);
    var datedefinnum = datedefin.getTime();*/
    var datedefinnum = String(<?php echo $default_date_fin; ?>);
    
    //alert(datedefinnum);

    if(countDownDate <= now){
        //setCookie("datedefinnum",datedefinnum,300);
        var countDownDate = datedefinnum; 
        //alert(datedefinnum);
    }
    else{
        var countDownDate = getCookie("datedefinnum");
        //var countDownDate23 = new Date(<?php echo $default_date_fin; ?>).getTime();
        //var countDownDate = String(countDownDate23);
        /*console.log((countDownDate));
        console.log(String(countDownDate23));*/
    }
    
    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get todays date and time
        //var now = new Date().getTime();
        var now = new Date(<?php echo strtotime("now"); ?>).getTime();
        //alert(now);

        // Find the distance between now an the count down date
        var distance = countDownDate - now;
        
        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="xx1"
        document.getElementById("jours").innerHTML = days;
        document.getElementById("heures").innerHTML = hours;
        document.getElementById("minutes").innerHTML = minutes;
        document.getElementById("secondes").innerHTML = seconds;

        // Display the result in the element with id="xx2"
       /* document.getElementById("jours2").innerHTML = days;
        document.getElementById("heures2").innerHTML = hours;
        document.getElementById("minutes2").innerHTML = minutes;
        document.getElementById("secondes2").innerHTML = seconds;

        // Display the result in the element with id="xx3"
        document.getElementById("jours3").innerHTML = days;
        document.getElementById("heures3").innerHTML = hours;
        document.getElementById("minutes3").innerHTML = minutes;
        document.getElementById("secondes3").innerHTML = seconds;

        // Display the result in the element with id="xx4"
        document.getElementById("jours4").innerHTML = days;
        document.getElementById("heures4").innerHTML = hours;
        document.getElementById("minutes4").innerHTML = minutes;
        document.getElementById("secondes4").innerHTML = seconds;*/

        // If the count down is finished, write some text 
        if (distance < 0) {
          clearInterval(x);
          document.getElementById("fin").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";
          //document.getElementById("fin2").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";
          //document.getElementById("fin3").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";
          //document.getElementById("fin4").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";
        }
    }, 1000);
</script>
<?php

