<?php
/*
Plugin Name: Liste contacte
Description: Liste tous le contacte
Version:     0.0.1
Author:      Fréderic Myagency
*/

add_shortcode( 'liste_contact', 'afficher_liste_contact');

function afficher_liste_contact(){
    if(!is_admin()){
        ob_start();
        global $wpdb;
        require_once 'vendor/autoload.php';
        
        // Demande authorisation s'il n'y a pas encore
        $url_to_redirect = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $infusionsoft = new \Infusionsoft\Infusionsoft(array(
                'clientId'     => 'uj3xp8vchkm87th38ttkr7pr',
                'clientSecret' => 'K37DzytKQr',
                'redirectUri'  => 'http://duroy.webero.fr/teste-liste-contact/',
        ));
        
        if (isset($_GET['code']) and !$infusionsoft->getToken()) {
            $_SESSION['token'] = ($infusionsoft->requestAccessToken($_GET['code']));
            $resus = $wpdb->insert(
                $wpdb->prefix . 'tokens',
                array(
                    'tokens'  => $_SESSION['token']->accessToken,
                    'tokens_refresh'        => $_SESSION['token']->refreshToken,
                    'lifetime'        => $_SESSION['token']->endOfLife,
                    'tokens_json'       => serialize($_SESSION['token']),
                )
            );
        }
        
        /*
          if (isset($_SESSION['token'])) {
                $infusionsoft->setToken(unserialize($_SESSION['token']));
          }
         if ($infusionsoft->getToken()) {
                $_SESSION['token'] = serialize($infusionsoft->getToken());
        } else {
                echo '<a href="' . $infusionsoft->getAuthorizationUrl() . '">Click here to authorize</a>';
        }*/
       
        /******* Processus pour le rafraichissement **********/
        $infusionrow    = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens');
        $accessToken    = $infusionrow->tokens;
        $refreshToken   = $infusionrow->tokens_refresh;
        $tmstmpnow      = $infusionrow->lifetime;
        $timestampn     = date('U');
       
        // test si la durée de vie du token est expiré ou non puis on rafraichi et mettre à jour le données dans la base de données
        //if((int)$timestampn  == ((int)$tmstmpnow-60)){
           /* $old_token = new \Infusionsoft\Token();
            $old_token->setAccessToken($accessToken);
            $old_token->setRefreshToken($refreshToken);
            $infusionsoft->setToken($old_token);
            $infusionsoft->refreshAccessToken();
            $infstToken = $infusionsoft->getToken();
            $data =array('tokens'=>$infstToken->getAccessToken(),'tokens_refresh'=>$infstToken->getRefreshToken(),'lifetime'=>$infstToken->getEndOfLife());
            $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
            $resMaxId = $sqlMaxId->ids;
            $where = array('id'=>(int)$resMaxId);
            $wpdb->update($wpdb->prefix.'tokens', $data, $where);*/
        //}
       
        // Résultat
        $resu = get_from_url();
        $arr_datas = array();
        if(!empty($resu['contacts'])){
            foreach($resu['contacts'] as $resData){
               $arr_datas[] = $resData;
            }
            require dirname( __FILE__ ) . '/template/index.php';
        }else{
            echo '<a href="' . $infusionsoft->getAuthorizationUrl() . '">Demander une authorisation</a>';
        }
        return ob_get_clean();
    }
}

add_action('wp_enqueue_scripts','script_contact');
function script_contact() {
    wp_enqueue_style( 'css_datatable', plugins_url( '/css/bootstrap.css', __FILE__ ),false,'6','all');
    wp_enqueue_style( 'css_datatableb', plugins_url( '/css/datatable/dataTables.bootstrap.min.css', __FILE__ ),false,'6','all');
    wp_enqueue_script( 'script_contact_datatable_boot', plugins_url( '/js/bootstrap.min.js', __FILE__ ),array ( 'jquery' ), 7, true);
    wp_enqueue_script( 'script_contact_datatable', plugins_url( '/js/datatable/jquery.dataTables.min.js', __FILE__ ),array ( 'jquery' ), 7, true);
    wp_enqueue_script( 'script_contact_datatable_b', plugins_url( '/js/datatable/dataTables.bootstrap.min.js', __FILE__ ),array ( 'jquery' ), 7, true);
    wp_enqueue_script( 'script_contact', plugins_url( '/js/script.js', __FILE__ ),array ( 'jquery' ), 6, true);
}

add_action('wp_ajax_get_detail_contact', 'get_detail_contact');
add_action('wp_ajax_nopriv_get_detail_contact', 'get_detail_contact');
/**
 * Ajax information client
 */
function get_detail_contact() {
    $refcl      = $_GET['id_demande'];
    $resu = get_from_url($refcl);
    $aff =  "<b>Nom : </b>".($resu["family_name"])."<br><br>";
    $aff .= "<b>Addresse email : </b>".($resu["email_addresses"][0]['email'])."<br><br>";
    $aff .= "<b>Date de création : </b>".(date('d-m-Y h:i:s', strtotime($resu["date_created"])))."<br><br>";
    $aff .= "<b>Date de modification : </b>".(date('d-m-Y h:i:s', strtotime($resu["last_updated"])))."<br><br>";
    $aff .= "<b>Tag ids : </b>".(implode(',',$resu["tag_ids"]))."<br><br>";
    $aff .= "<b>Email status : </b>".($resu["email_status"])."<br><br>";
    $aff .= "<b>Numéros de télephone : </b>".($resu["phone_numbers"][0]['number'])."<br><br>";
    echo $aff;
    die();
}


/**
 * Curl get
 * @global type $wpdb
 * @param type $param
 * @return type
 */
function get_from_url($param = ""){
    global $wpdb;
    $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
    $resMaxId = $sqlMaxId->ids;
    $infusionrow = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
    $accessToken = $infusionrow->tokens;
    if(!empty($param)){
        $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts/".$param."?access_token=".$accessToken;
    }else{
        $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts?order=id&order_direction=descending&access_token=".$accessToken;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$urls);
    $result=curl_exec($ch);
    curl_close($ch);
    $resu = (json_decode($result, true));
    return $resu;
}

/**
 * Curl post
 * @param type $url
 * @param type $data
 * @return type
 */
function post_to_url($url, $data) {
    $fields = '';
    foreach ($data as $key => $value) {
        $fields .= $key . '=' . $value . '&';
    }
    rtrim($fields, '&');
    $post = curl_init();
    curl_setopt($post, CURLOPT_URL, $url);
    curl_setopt($post, CURLOPT_POST, count($data));
    curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($post);
    curl_close($post);
    return $result;
}

register_activation_hook( __FILE__, 'tokens_install' );
/**
 * Installation table pour stocké le tokens
 * @global type $wpdb
 * @global string $references_db_version
 */
function tokens_install() {
	global $wpdb;
	global $references_db_version;
        $references_db_version = '1.0';
	$table_name      = $wpdb->prefix . 'tokens';
	$charset_collate = $wpdb->get_charset_collate();
	$sql = "CREATE TABLE ".$wpdb->prefix."tokens (
		id mediumint(9) NOT NULL AUTO_INCREMENT,
		tokens text,
		tokens_refresh text,
		lifetime text,
		tokens_json text,
		PRIMARY KEY  (id)
	) $charset_collate;";
	require_once(ABSPATH.'wp-admin/includes/upgrade.php');
	dbDelta( $sql );
	add_option('tokens_db_version',$references_db_version);
}





