<?php
/*
Plugin Name: Widget décompteur
Description: Widget décompteur
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/



function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );

function connect_to_infusion_soft(){
    global $wpdb;
    include ABSPATH . "/vendor/autoload.php";
    $infusionsoft = new \Infusionsoft\Infusionsoft(array(
                'clientId'     => 'uj3xp8vchkm87th38ttkr7pr',
                'clientSecret' => 'K37DzytKQr',
                'redirectUri'  => 'http://duroy.webero.fr/teste-liste-contact/',
    ));
    $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
    $resMaxId = $sqlMaxId->ids;
    $infusionrow    = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
    $accessToken    = $infusionrow->tokens;
    $refreshToken   = $infusionrow->tokens_refresh;
    $endtimeToken   = (int)$infusionrow->lifetime;
    $token_in_database = new \Infusionsoft\Token();
    $token_in_database->setAccessToken($accessToken);
    $token_in_database->setRefreshToken($refreshToken);
    $token_in_database->setEndOfLife($endtimeToken);
    $infusionsoft->setToken($token_in_database);
    return $infusionsoft;
}

function set_date_debut_decompte($contactID){
    $infusionsoft   = connect_to_infusion_soft();
    $datetimeNow = new \DateTime('now',new \DateTimeZone('Europe/Paris'));
    $updated_field_date_now = array('_Datededebutdudecompte' => $datetimeNow);
    $infusionsoft->contacts('xml')->update($contactID, $updated_field_date_now);
}

function get_date_debut_decompte($contactID){
    $infusionsoft   = connect_to_infusion_soft();
    $returnFields   = array('_Datededebutdudecompte');
    //$contact_id = isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"];
    $resdate        = $infusionsoft->contacts('xml')->load($contactID, $returnFields);
    return $resdate;
}

function set_date_fin_decompte($contactID,$new_date=""){
    $infusionsoft   = connect_to_infusion_soft();
    
    if(empty($new_date)){
        $updated_field_date = array('_Datedefindudecompte' => calcul_date_aujourd_hui_plus_7_plus_hms_minuit());
    }else{
        $updated_field_date = array('_Datedefindudecompte' => $new_date);
    }
    
    $infusionsoft->contacts('xml')->update($contactID, $updated_field_date);
}

function get_date_fin_decompte($contactID){
    $infusionsoft   = connect_to_infusion_soft();
    $returnFields   = array('_Datedefindudecompte');
    $resdate        = $infusionsoft->contacts('xml')->load($contactID, $returnFields);
    return $resdate;
}



function set_nombre_jour_offre($contactID,$valnbjouroffre){
    $infusionsoft   = connect_to_infusion_soft();
    $returnFields   = array('_Nombredejourdeloffre0'=>$valnbjouroffre);
    $infusionsoft->contacts('xml')->update($contactID, $returnFields);
   // return 1;
}

function get_nombre_jour_offre($contactID){
    $infusionsoft   = connect_to_infusion_soft();
    $returnFields   = array('_Nombredejourdeloffre0');
    $resnboffre        = $infusionsoft->contacts('xml')->load($contactID, $returnFields);
    return $resnboffre;
}

function get_date_derniere_visite($contactID){
    $infusionsoft   = connect_to_infusion_soft();
    $returnFields   = array('_Datededernièrevisitedudecompte');
    $resdate        = $infusionsoft->contacts('xml')->load($contactID, $returnFields);
    return $resdate;
}

function set_date_derniere_visite($contactID){
    $infusionsoft   = connect_to_infusion_soft();
    $datetimeNow = new \DateTime('now',new \DateTimeZone('Europe/Paris'));
    $updated_field_date_dern = array('_Datededernièrevisitedudecompte' => $datetimeNow);
    $infusionsoft->contacts('xml')->update($contactID, $updated_field_date_dern);
}

function calcul_date_aujourd_hui_plus_7_plus_hms_minuit($flagjs="",$nbjours=""){
    $Today = date('d-m-Y H:i:s');
    $todayDatetime = new Datetime($Today);
    $midnight = strtotime("tomorrow 00:00:00");
    $dt       = new DateTime('@' . $midnight);
    $intervals = $todayDatetime->diff($dt);
    $resHoursMS = ($intervals->format('%h:%i:%s'));
    $arrbeforMinuit = explode(':',$resHoursMS);
    if(!empty($flagjs)){
        if(!empty($nbjours)){
            $restday = $nbjours;
        }else{
            $restday = 7;
        }
    }else{
        $restday = 6;
    }
    $NewDate = Date('d-m-Y H:i:s', strtotime("+".$restday." days +".(int)$arrbeforMinuit[0]." hour +".(int)$arrbeforMinuit[1]." minutes +".(int)$arrbeforMinuit[2]." seconds"));
    $dateTime7jours = new Datetime($NewDate);
    return $dateTime7jours;
}

function get_from_url_contact($param = ""){
    global $wpdb;
    $sqlMaxId = $wpdb->get_row('SELECT max(id) as ids FROM '.$wpdb->prefix.'tokens');
    $resMaxId = $sqlMaxId->ids;
    $infusionrow = $wpdb->get_row('SELECT * FROM '.$wpdb->prefix.'tokens where id='.$resMaxId);
    $accessToken = $infusionrow->tokens;
    if(!empty($param)){
        $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts/".$param."?access_token=".$accessToken;
    }else{
        $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts?order=id&order_direction=descending&access_token=".$accessToken;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$urls);
    $result=curl_exec($ch);
    curl_close($ch);
    $resu = (json_decode($result, true));
    return $resu;
}

add_action('wp_ajax_set_fin_offre', 'set_fin_offre');
add_action('wp_ajax_nopriv_set_fin_offre', 'set_fin_offre');
function set_fin_offre() {
    $contact_id = (int)$_GET["contactId"];
    set_date_derniere_visite($contact_id);
}

class wpb_widget extends WP_Widget {
 
    //private $contact_id = 13;
    private $contact_id;
    
    function __construct() {
        parent::__construct(
            'wpb_widget', 
            'Widget décompteur', 
            array( 'description' => "Décompteur offre", ) 
            );
        
            $this->contact_id = isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"];
    }
    
    // Affichage du widget dans front office
    public function widget( $args, $instance ) {
        // variable afficher dans le template à ne pas supprimer
        $nombre_de_jour                = apply_filters( 'widget_title', $instance['title'] );
        $nombre_de_jour_fin_par_defaut = calcul_date_aujourd_hui_plus_7_plus_hms_minuit('js',$nombre_de_jour);
        
        echo $args['before_widget'];
        //echo $args['before_title'] . $title . $args['after_title'];
        if(isset($this->contact_id) || !empty($this->contact_id)){
            // mise à jour date debut consultation
            $date_debut_decompte = get_date_debut_decompte($this->contact_id);
            if(empty($date_debut_decompte)){
                set_date_debut_decompte($this->contact_id);
            }

            // mise à jour nombre jour offre
            $nombre_jour_offre = get_nombre_jour_offre($this->contact_id)['_Nombredejourdeloffre0'];
            if(empty($nombre_jour_offre)){
                set_nombre_jour_offre($this->contact_id,7);
            }

            // mise à jour date fin
            $date_fin_decompte = get_date_fin_decompte($this->contact_id);
            if(empty($date_fin_decompte)){
                set_date_fin_decompte($this->contact_id);
                $default_date_fin              = $nombre_de_jour_fin_par_defaut->getTimestamp();
            }else{
                $resutoJS           = $date_fin_decompte['_Datedefindudecompte']->add(new DateInterval('P1D'));
                $default_date_fin   = $resutoJS->getTimestamp();
            }
            
            require dirname( __FILE__ ) . '/template/index.php';
        }
        echo $args['after_widget'];
    }

    // Affichage Backend 
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }else {
            $title = __( 'New title', 'wpb_widget_domain' );
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Nombre de jour:' ); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="number" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php 
    }
    
    // Mise à jour 
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        $resu = get_from_url_contact();
        if(!empty((int)$new_instance['title'])){
            foreach($resu['contacts'] as $resData){
                $date_debut_decompte = get_date_debut_decompte($resData['id']);
                if(!empty($date_debut_decompte)){
                    set_nombre_jour_offre($resData['id'],(int)$new_instance['title']);
                    $updates = $date_debut_decompte['_Datededebutdudecompte']->modify( '+'.(int)$new_instance['title'].' day' );
                    set_date_fin_decompte($resData['id'],$updates);
                }
            }
        }
       
        return $instance;
    }
} 


