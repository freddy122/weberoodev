
<div class="clockdiv">
  <div>
    <span class="jours" id="jours">7</span>
    <div class="smalltext">Jours</div>
  </div>
  <div>
    <span class="heures" id="heures">00</span>
    <div class="smalltext">Heures</div>
  </div>
  <div>
    <span class="minutes" id="minutes">00</span>
    <div class="smalltext">Minutes</div>
  </div>
  <div>
    <span class="secondes" id="secondes">00</span>
    <div class="smalltext">Secondes</div>
  </div>
</div>
<div id="finoffre" class="finoffre">
</div>


<style>
#demo{
    display: none;
}
h1{
  color: #396;
  font-weight: 100;
  font-size: 30px;
  margin: 40px 0px 20px;
}

#clockdiv{
	font-family: sans-serif;
	color: #fff;
	display: inline-block;
	font-weight: 100;
	text-align: center;
	font-size: 26px;
	width: 100%;
}

#clockdiv > div{
	padding: 10px;
	border-radius: 3px;
	background: #4584CC;
	display: inline-block;
}

#clockdiv div > span{
	padding: 10px;
	border-radius: 3px;
	background: #0056B9;
	display: inline-block;
}

.smalltext{
	padding-top: 5px;
	font-size: 12px;
}
</style>


<script>
//GET cookie
    function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//SET cookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

//debut code 
    var now = parseInt(new Date(<?php echo strtotime("now"); ?>).getTime());
    var countDownDate = now;
    //var countDownDate = getCookie("datedefinnum");
    var countDownDate = new Date(<?php echo $default_date_fin; ?>).getTime();
   
function beforeMidnight(){
    var mid= new Date(), 
    ts= mid.getTime();
    mid.setHours(24, 0, 0, 0);
    return Math.floor((mid - ts));
}
//alert (beforeMidnight());
restejournee = beforeMidnight();
//alert (restejournee);
   
   
   
    var days = <?php echo $nombre_jour_offre; ?>;
    var date = parseInt(new Date(<?php echo strtotime("now"); ?>).getTime()*1000);
    var res = restejournee + date + (days * 24 * 60 * 60 * 1000);
    var datedefin = new Date(res);
    var datedefinnum = datedefin.getTime();
    //alert(datedefinnum);

    if(countDownDate <= now){
       /* setCookie("datedefinnum",datedefinnum,300);
        var countDownDate = datedefinnum; */
        var countDownDate = datedefinnum; 
        //alert(datedefinnum);
    } else {
        //var countDownDate = getCookie("datedefinnum");
        var countDownDate = parseInt(new Date(<?php echo $default_date_fin; ?>).getTime());
        //console.log(countDownDate);
    }
    
// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();
  //alert(now);

  // Find the distance between now an the count down date
  var distance = parseInt(countDownDate*1000) - now;
  //var distance = -1222;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="xx1"
 /* document.getElementById("jours").innerHTML = days;
  document.getElementById("heures").innerHTML = hours;
  document.getElementById("minutes").innerHTML = minutes;
  document.getElementById("secondes").innerHTML = seconds;*/
  jQuery(".jours").html(days);
  jQuery(".heures").html(hours);
  jQuery(".minutes").html(minutes);
  jQuery(".secondes").html(seconds);
    // Display the result in the element with id="xx2"
  /*document.getElementById("jours2").innerHTML = days;
  document.getElementById("heures2").innerHTML = hours;
  document.getElementById("minutes2").innerHTML = minutes;
  document.getElementById("secondes2").innerHTML = seconds;
  
    // Display the result in the element with id="xx3"
  document.getElementById("jours3").innerHTML = days;
  document.getElementById("heures3").innerHTML = hours;
  document.getElementById("minutes3").innerHTML = minutes;
  document.getElementById("secondes3").innerHTML = seconds;
  
      // Display the result in the element with id="xx4"
  document.getElementById("jours4").innerHTML = days;
  document.getElementById("heures4").innerHTML = hours;
  document.getElementById("minutes4").innerHTML = minutes;
  document.getElementById("secondes4").innerHTML = seconds;*/

  // If the count down is finished, write some text 
  if (distance < 0) {
    clearInterval(x);
    jQuery(".finoffre").html("<h3>L'offre est terminée. N'hésitez pas à prendre contact avec nous.</h3>");
    jQuery(".clockdiv").hide();
    finOffre();
    /*document.getElementById("fin2").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";
    document.getElementById("fin3").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";
    document.getElementById("fin4").innerHTML = "L'offre est terminée. N'hésitez pas à prendre contact avec nous.";*/
  }
}, 1000);

function finOffre(){
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'set_fin_offre',contactId:'<?php echo isset($_COOKIE["contactId"]) ? $_COOKIE["contactId"]:$_GET["contactId"]; ?>'},
            success: function (res) {
                //console.log(res);
                jQuery(".clockdiv").hide();
            }
        });
        
}
</script>
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

