<?php
/*
Plugin Name: Liste contactes
Description: Liste toutes les contactes
Version:     0.0.1
Author:      Orimbatosoa fréderic
*/

add_shortcode( 'liste_contact', 'afficher_liste_contact');

function afficher_liste_contact(){
    if(!is_admin()){
        ob_start();
        $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts?access_token=pkgsds7d9c4afcvzz5wmvz7u";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL,$urls);
        $result=curl_exec($ch);
        curl_close($ch);
        $resu = (json_decode($result, true));
        $arr_datas = array();
        foreach($resu['contacts'] as $resData){
           $arr_datas[] = $resData;
        }
        
        /*************** ou je peu utiliser file_get_contents**********/
        /*$result = file_get_contents("https://api.infusionsoft.com/crm/rest/v1/contacts?access_token=pkgsds7d9c4afcvzz5wmvz7u");
        var_dump(json_decode($result, true));*/
        require dirname( __FILE__ ) . '/template/index.php';
        return ob_get_clean();
    }
}

add_action('wp_enqueue_scripts','script_contact');

function script_contact() {
    //wp_enqueue_script( 'script_contact_jquery', plugins_url( '/js/jquery-1.10.2.js', __FILE__ ),array ( 'jquery' ), 1.1, true);
    //wp_enqueue_style( 'css_datatable', plugins_url( '/css/custom_css.css', __FILE__ ),false,'6','all');
    wp_enqueue_style( 'css_datatable', plugins_url( '/css/bootstrap.css', __FILE__ ),false,'6','all');
    wp_enqueue_style( 'css_datatable', plugins_url( '/css/datatable/dataTables.bootstrap.min.css', __FILE__ ),false,'6','all');
    wp_enqueue_script( 'script_contact_datatable_boot', plugins_url( '/js/bootstrap.min.js', __FILE__ ),array ( 'jquery' ), 7, true);
    wp_enqueue_script( 'script_contact_datatable', plugins_url( '/js/datatable/jquery.dataTables.min.js', __FILE__ ),array ( 'jquery' ), 7, true);
    wp_enqueue_script( 'script_contact_datatable_b', plugins_url( '/js/datatable/dataTables.bootstrap.min.js', __FILE__ ),array ( 'jquery' ), 7, true);
    wp_enqueue_script( 'script_contact', plugins_url( '/js/script.js', __FILE__ ),array ( 'jquery' ), 6, true);
}

add_action('wp_ajax_get_detail_contact', 'get_detail_contact');
add_action('wp_ajax_nopriv_get_detail_contact', 'get_detail_contact');
function get_detail_contact() {
    $refcl      = $_GET['id_demande'];
    $urls = "https://api.infusionsoft.com/crm/rest/v1/contacts/".$refcl."?access_token=pkgsds7d9c4afcvzz5wmvz7u";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$urls);
    $result=curl_exec($ch);
    curl_close($ch);
    $resu = (json_decode($result, true));
   // var_dump($resu);
    echo "<b>Nom : </b>".($resu["family_name"])."<br><br>";
    echo "<b>Addresse email : </b>".($resu["email_addresses"][0]['email'])."<br><br>";
    echo "<b>Date de création : </b>".(date('d-m-Y h:i:s', strtotime($resu["date_created"])))."<br><br>";
    echo "<b>Date de modification : </b>".(date('d-m-Y h:i:s', strtotime($resu["last_updated"])))."<br><br>";
    echo "<b>Tag ids : </b>".(implode(',',$resu["tag_ids"]))."<br><br>";
    
    echo "<b>Email status : </b>".($resu["email_status"])."<br><br>";
    echo "<b>Numéros de télephone : </b>".($resu["phone_numbers"][0]['number'])."<br><br>";
   die();
}


