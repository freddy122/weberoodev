<!-- Trigger the modal with a button -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body">
        <div id="content_modals" style="padding: 15px">
            sfes
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div id="loader_img"  style="display:none;">
        <img src="<?php echo (plugins_url( 'images/ajax-loading.gif', __FILE__));?>"  style="width:15%;">
    </div>
<table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nom</th>
                <th>Date création</th>
                <th>Action</th>
               
            </tr>
        </thead>
        <tbody>
            <?php foreach($arr_datas as $resus): ?>
            <tr>
                <td><?php echo $resus['id']; ?></td>
                <td><?php echo $resus['family_name']; ?></td>
                <td><?php echo date('d-m-Y h:i:s', strtotime($resus['date_created'])); ?></td>
                <td><button type="button" class="btn btn-info" onclick="showDetail('<?php echo $resus['id']; ?>')">Détail</button></td>
            </tr>
            <?php endforeach; ?>
            
        </tbody>
</table>
<script>
    jQuery(document).ready(function(){
    jQuery('#example').DataTable();
    
});

function showDetail(id_demande){
    jQuery("#loader_img").show();
        jQuery.ajax({
            type: "GET",
            url: '<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php',
            data: {action : 'get_detail_contact',id_demande : id_demande},
            success: function (res) {
                jQuery("#loader_img").hide();
                jQuery('#myModal').find('#content_modals').html(res);
                jQuery('#myModal').modal('show');
            }
        });
        
    }
</script>
<?php


